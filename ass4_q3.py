# define class with constructor
class Student:
  def __init__(self, name, test_score, space_invaders_score):
    self.name = name
    self.test_score = test_score
    self.space_invaders_score = space_invaders_score

# read file
x = open('datafile2.txt', 'r')

# init empty list
students = []

for line in x:
  val = line.split()
  students.append(Student(val[0], val[1], val[2]))

# test scores of 3rd student
print('The 3rd student scored {0} on the test.\n'.format(students[2].test_score))

# name and space invader score of 5th student
print('The 5th student is named {0} and has a Space Invader score of {1}.\n'.format(students[4].name, students[4].space_invaders_score))

# close file
x.close()

