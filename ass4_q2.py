import matplotlib.pyplot as plt

# read contents of text file
x = open('datafile2.txt', 'r')

# init vars
sum_test_score = 0
sum_invader_score = 0
x_axis = []
y_axis = []

# iterate
for line in x:
  val = line.split()

  # add values
  sum_test_score = sum_test_score + int(val[1])
  sum_invader_score = sum_invader_score + int(val[2])

  # set axis arrays values
  x_axis.append(int(val[1]))
  y_axis.append(int(val[2]))

# set mean according to # of students
mean_test_score = sum_test_score/10
mean_invader_score = sum_invader_score/10
print('mean test score: {0}'.format(mean_test_score))
print('mean space invaders score: {0}'.format(mean_invader_score))

plt.scatter(y_axis, x_axis)
plt.title('Test scores according to Space Invader scores')
plt.xlabel('Space Invader scores')
plt.ylabel('Test scores')
plt.show()

# close file
x.close()