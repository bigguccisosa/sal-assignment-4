# define class
class Taxonomy:
  def __init__(self, Phylum, Class, Order, Family, Genus, Species):
    self.Phylum = Phylum
    self.Class = Class
    self.Order = Order
    self.Family = Family
    self.Genus = Genus
    self.Species = Species

# read file
x = open('taxonomy.txt', 'r')

# skip first line
y = iter(x)
next(y)

# init list
species = []

# create objects
for line in y:
  values = line.split()
  species.append(Taxonomy(values[0], values[1], values[2], values[3], values[4], values[5]))

# a
print('Species in genus Pseudomonas:')
pseudomonas_species = []
for obj in species:
  if obj.Genus == 'Pseudomonas':
    pseudomonas_species.append(obj.Species)
print(pseudomonas_species)

# b
print('\nNames of all unique genera:')
genera = []
for obj in species:
  genera.append(obj.Genus)
print(list(dict.fromkeys(genera)))

# c
print('\nTaxonomic information backwards:')
for obj in species:
  print('{0}, {1}, {2}, {3}, {4}, {5}'.format(obj.Species, obj.Genus, obj.Family, obj.Order, obj.Class, obj.Phylum))

# close file
x.close()